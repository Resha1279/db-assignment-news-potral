"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Post extends Model {
    static associate(models) {
      this.hasOne(models.User, { foreignKey: "u_id", allowNull: false });
    }
  }
  Post.init(
    {
      p_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      p_title: DataTypes.STRING,
      p_type: DataTypes.STRING,
      p_description: DataTypes.STRING,
      p_status: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Post",
    }
  );
  return Post;
};
