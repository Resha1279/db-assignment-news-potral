"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class News extends Model {
    static associate(models) {
      this.hasOne(models.Category, { foreignKey: "c_id", allowNull: false });
    }
  }
  News.init(
    {
      n_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      n_title: DataTypes.STRING,
      n_type: DataTypes.STRING,
      n_description: DataTypes.STRING,
      n_status: DataTypes.STRING,
      category_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "News",
    }
  );
  return News;
};
