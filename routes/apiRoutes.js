const { request, response } = require("express");
const express = require("express");
const router = express.Router();
const db = require("../models");

router.get("/users", (request, response) => {
  db.User.findAll().then((users) => {
    response.send(users);
  });
});

router.get("/categories", (request, response) => {
  db.Category.findAll().then((categories) => {
    response.send(categories);
  });
});

router.get("/news", (request, response) => {
  db.News.findAll().then((news) => {
    response.send(news);
  });
});

router.get("/posts", (request, response) => {
  db.Post.findAll().then((posts) => {
    response.send(posts);
  });
});

router.post("/create-user", (request, response) => {
  db.User.create({
    u_firstname: request.body.firstname,
    u_lastname: request.body.lastname,
    u_email: request.body.email,
    u_password: request.body.password,
  }).then((createdUser) => response.send(createdUser));
});

router.post("/create-post", (request, response) => {
  db.Post.create({
    p_title: request.body.title,
    p_type: request.body.type,
    p_description: request.body.description,
    p_status: request.body.status,
    user_id: request.body.user_id,
  }).then((createdPost) => response.send(createdPost));
});

router.post("/create-news", (request, response) => {
  db.News.create({
    n_title: request.body.title,
    n_type: request.body.type,
    n_description: request.body.description,
    n_status: request.body.status,
    category_id: request.body.category_id,
  }).then((createdNews) => response.send(createdNews));
});

router.post("/create-category", (request, response) => {
  db.Category.create({
    c_name: request.body.name,
  }).then((createdCategory) => response.send(createdCategory));
});

router.delete("/delete-user/:id", (request, response) => {
  db.User.destroy({
    where: {
      u_id: request.params.id,
    },
  }).then(() => response.send("delete success"));
});

router.put("/edit-user/:id", (request, response) => {
  db.User.update(
    {
      u_firstname: request.body.firstname,
      u_lastname: request.body.lastname,
      u_email: request.body.email,
      u_password: request.body.password,
    },
    {
      where: {
        u_id: request.params.id,
      },
    }
  ).then(() => response.send("Edit Success"));
});

module.exports = router;
